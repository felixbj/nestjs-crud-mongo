import { seeder } from "nestjs-seeder";
import { MongooseModule } from "@nestjs/mongoose";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { MONGOOSE_CONFIG } from "./config/constats";
import { Rol,RolSchema } from "./modules/rol/schema";
import { Profile,ProfileSchema,User,UserSchema } from "./modules/user/schema";
import { RolsSeeder,AdminSeeder } from "./database/seeders";
import databaseConfig from "./config/database.config";
 
seeder({
  imports: [
    ConfigModule.forRoot({
      load: [databaseConfig],// cargar la confguracon de la base de datos --- varble de entorno llamada database
      isGlobal: true,
      envFilePath: '.env'
    }),
    MongooseModule.forRootAsync({
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory:async (configService:ConfigService) => configService.get(MONGOOSE_CONFIG)
    }),
    MongooseModule.forFeature([
        { name:Rol.name,schema:RolSchema },
        { name:User.name,schema:UserSchema },
        { name:Profile.name,schema:ProfileSchema },
    ])
  ]
}).run([RolsSeeder,AdminSeeder]);