import { BadRequestException,ConflictException,Injectable,NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateUserDto, ReadUserDto, UpdateUserDto } from './dto';
import { User,UserDocument,Profile,ProfileDocument } from './schema';
import {genSalt, hash} from 'bcryptjs';
import { BCRYPT_SALT,DEFAULT_ROL } from 'src/config/constats';
import { plainToClass } from 'class-transformer';
import { Rol,RolDocument } from '../rol/schema';

@Injectable()
export class UserService {
    constructor(
        @InjectModel(User.name)
        private readonly userModel: Model<UserDocument>,
        @InjectModel(Profile.name)
        private readonly profileModel: Model<ProfileDocument>,
        @InjectModel(Rol.name)
        private readonly rolModel: Model<RolDocument>,
        //private readonly mongoose: Mongoose
    ) {}
    async getAll():Promise<ReadUserDto[]>{
        //if(!this.mongoose.connection.readyState) throw new BadRequestException();
        return (await this.userModel.find()).map(
            (user:User) => plainToClass(ReadUserDto,user)// class-transformer pasar la clase user a un dto
        );
    }
    async getOne(id:string,):Promise<ReadUserDto>{
        if (!id) throw new BadRequestException();
        const user = await this.userModel.findOne({'_id':id});
        if (!user) throw new NotFoundException()
        return plainToClass(ReadUserDto,user);
    }
    async createOne(user:CreateUserDto):Promise<ReadUserDto>{
        if(!user) throw new BadRequestException()
        const f =  await this.userModel.findOne().or([
            { username: user.username },
            { email: user.email }
        ]).then(user => { 
            return (user)?true:false;
        }).catch(error => { return false; });
        if(f) throw new ConflictException()
        const rol = await this.rolModel.findOne({'access':DEFAULT_ROL,'isActive':true});
        if(!rol) throw new ConflictException()
        const newUser = new this.userModel(Object.assign({},user,{ role:[rol] },{ password:await hash (user.password, await genSalt (BCRYPT_SALT)) }));
        const profile = new this.profileModel({ idUser:newUser });
        await newUser.save(function(err, doc) {
            if (err) throw new NotFoundException(err)
        });
        await profile.save(function(err, doc) {
            if (err) throw new NotFoundException(err)
        });
        return plainToClass(ReadUserDto,newUser);
    }
    async editOne(id:string,user:UpdateUserDto):Promise<ReadUserDto>{
        if(!user) throw new BadRequestException()
        const data = await this.userModel.findOne({'_id':id});
        if (!data) throw new NotFoundException()
        const editedUser = (!user.hasOwnProperty('password'))?Object.assign(data,user):Object.assign(data,user,{ password:await hash(user.password,await genSalt(BCRYPT_SALT)) });
        return plainToClass(ReadUserDto, await editedUser.save());
    }
    async markDeleteOne(id:string):Promise<ReadUserDto>{
        if (!id) throw new BadRequestException();
        const user = await this.userModel.findOne({'_id':id});
        if (!user) throw new NotFoundException()
        const editedUser = Object.assign(user,{ isActive: false });//marca de elimnado
        return plainToClass(ReadUserDto, await editedUser.save());
    }
    async deleteOne(id:string):Promise<any>{
        if (!id) throw new BadRequestException();
        const data = await this.userModel.findOne({ '_id':id,isActive:false });
        if (!data) throw new NotFoundException()
        const deletedUser = await this.userModel.findOne({'_id':id});
        const deletedProfile = await this.profileModel.findOne({'idUser':deletedUser});
        return await deletedUser.remove(function(err, doc) {
            if (err) throw new NotFoundException(err)
            deletedProfile.remove();
        })
    }
}
