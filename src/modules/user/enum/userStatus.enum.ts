export enum UserStatus{
    'ACTIVE',
    'INACTIVE',
    'LOCKED',
    'REMOVED'
}