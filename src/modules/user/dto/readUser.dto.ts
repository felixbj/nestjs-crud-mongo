import { IsEmail, IsEnum, IsNumber, IsString } from "class-validator";
import { Exclude,Expose, Type } from "class-transformer"
import { UserStatus } from "../enum";
//import { ReadRolDto } from "../../rol/dto";

@Exclude()
export class ReadUserDto {
    @Expose()
    @IsString()
    readonly id: number;
    @Expose()
    @IsString()
    readonly username: string;
    @Expose()
    @IsString()
    @IsEmail()
    readonly email: string;
    @Expose()
    @IsString()
    @IsEnum(UserStatus)
    readonly status: UserStatus;
    /*@Expose()
    @Type(() => ReadRolDto)
    readonly role : ReadRolDto[];*/
}