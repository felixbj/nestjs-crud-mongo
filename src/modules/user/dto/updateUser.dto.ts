import { IsEmail,IsEnum,IsNotEmpty,IsOptional,Length } from "class-validator";
import { UserStatus } from "../enum";

export class UpdateUserDto {
    @IsNotEmpty()
    @IsOptional()
    @Length(3,50)
    readonly username: string;
    @IsNotEmpty()
    @IsEmail()
    @IsOptional()
    readonly email: string;
    @IsNotEmpty()
    @IsOptional()
    @Length(8,24)
    readonly password: string;
    @IsNotEmpty()
    @IsOptional()
    @IsEnum(UserStatus)
    readonly status: UserStatus;
    @IsNotEmpty()
    @IsOptional()
    readonly isActive: boolean;
}