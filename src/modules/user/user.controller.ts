import { Body,Controller,Delete,Get,Param,ParseIntPipe,Post,Put } from '@nestjs/common';
import { CreateUserDto,UpdateUserDto } from './dto';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
    constructor(
        private readonly userService:UserService
    ){}
    @Get()
    async getAllUser() {
        const data = await this.userService.getAll();
        return {
            message: 'Todos los usuarios',
            data
        }
    }
    @Get(':id')
    async getOneUser(@Param('id') idUser:string) {
        const data = await this.userService.getOne(idUser);
        return {
            message: `Un usuario con el id ${data.id}`,
            data
        }
    }
    @Post()
    async createOneUser(@Body() userDto:CreateUserDto){
        const data = await this.userService.createOne(userDto);
        return {
            message: 'Crea un usuario',
            data
        }
    }
    @Put(':id')
    async editOneUser(@Param('id') idUser:string,@Body() userDto:UpdateUserDto){
        const data = await this.userService.editOne(idUser,userDto);
        return {
            message: `Edita un solo usuario con el id ${data.id}`,
            data
        };
    }
    @Delete(':id')
    async markDeleteOneUser(@Param('id') idUser:string){
        const data = await this.userService.markDeleteOne(idUser);
        return {
            message: `Elimina un solo usuario con el id ${idUser}`,
            data
        };
    }
    @Delete('_delete/:id')
    async deleteOneUser(@Param('id') idUser:string){
        const data = await this.userService.deleteOne(idUser);
        return {
            message: `Elimina un solo usuario con el id ${idUser}`,
            data
        };
    }
}
