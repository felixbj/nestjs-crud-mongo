import { UserController } from './user.controller';
import { UserService } from './user.service';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { User,UserSchema,Profile,ProfileSchema } from './schema';
import { Mongoose } from 'mongoose';
import { Rol,RolSchema } from '../rol/schema';

@Module({
    imports: [
        Mongoose,
        MongooseModule.forFeature([
            { name:User.name,schema:UserSchema },
            { name:Profile.name,schema:ProfileSchema },
            { name:Rol.name,schema:RolSchema }
        ])
    ],
    controllers: [
        UserController,
    ],
    providers: [
        Mongoose,
        UserService
    ],
})
export class UserModule { }
// crear los directorios dto, enum y schema
//  crear en schema un schema para usuario y perfil
//  importar los schemas que usaremos en user
//  instalamos npm install bcryptjs