import { Prop,Schema,SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { User } from './user.schema';

export type ProfileDocument = Profile & mongoose.Document;

@Schema()
export class Profile {
    @Prop({ type:String })
    firstName: String;
    @Prop({ type:String })
    lastName: String;
    @Prop({ type:Date })
    birthdate: Date;
    @Prop({ type:mongoose.Schema.Types.ObjectId,ref:'user' })
    idUser: User;
    @Prop({ type:Date,default:Date.now })
    created_at: Date;
    @Prop({ type:Date })
    updated_at: Date;
}

export const ProfileSchema = SchemaFactory.createForClass(Profile);