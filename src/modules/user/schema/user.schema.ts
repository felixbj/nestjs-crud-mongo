import { Prop,Schema,SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { USER_ACTIVE } from 'src/config/constats';
import { Rol } from 'src/modules/rol/schema';

export type UserDocument = User & mongoose.Document;
/*export interface UserDocument extends mongoose.Document{
    username: string;
    email: string;
    password: string;
    status?: string;
    isActive?: Boolean;
    profile: Profile;
    role: Rol[];
}*/

@Schema()
export class User {
    @Prop({ type:String,index:true,unique:true,required:true })
    username: String;
    @Prop({ type:String,index:true,unique:true,required:true })
    email: String;
    @Prop({ type:String,required:true })
    password: String;
    @Prop({ type:String,default:USER_ACTIVE })
    status: String;
    @Prop({ type:Boolean,default:true })
    isActive: Boolean;
    @Prop([{ type:mongoose.Schema.Types.ObjectId,ref:'role' }])
    role: Rol[];
    @Prop({ type:Date,default:Date.now })
    created_at: Date;
    @Prop({ type:Date })
    updated_at: Date;
}

export const UserSchema = SchemaFactory.createForClass(User);