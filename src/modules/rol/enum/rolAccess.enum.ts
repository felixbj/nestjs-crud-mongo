export enum RolAccess{
    'ADMINISTER',
    'PUBLISH',
    'VIEWER'
}// para manejar acceso a nivel de aplicacion