import { RolController } from './rol.controller';
import { RolService } from './rol.service';
import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Rol,RolSchema } from './schema';

@Module({
    imports: [
        MongooseModule.forFeature([
            { name:Rol.name,schema:RolSchema },
        ])
    ],
    controllers: [
        RolController,
    ],
    providers: [
        RolService,
    ],
})
export class RolModule { }
// Importamos MoongoseModule forfeature para poder usarlo... 
