import { Prop,Schema,SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { DEFAULT_ROL, ROL_ACTIVE } from 'src/config/constats';

export type RolDocument = Rol & mongoose.Document;

@Schema()
export class Rol {
    @Prop({ type:String,index:true,unique:true })
    name: String;
    @Prop({ type:String })
    description: String;
    @Prop({ type:String,default:DEFAULT_ROL })
    access: String;
    @Prop({ type:String,default:ROL_ACTIVE })
    status: String;
    @Prop({ type:Boolean,default:true })
    isActive: Boolean;
    @Prop({ type:Date,default:Date.now })
    created_at: Date;
    @Prop({ type:Date })
    updated_at: Date;
}

export const RolSchema = SchemaFactory.createForClass(Rol);