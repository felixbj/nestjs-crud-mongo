import { Body,Controller,Delete,Get,Param,Post, Put } from '@nestjs/common';
import { CreateRolDto, UpdateRolDto } from './dto';
import { RolService } from './rol.service';

@Controller('rol')
export class RolController {
    constructor(
        private readonly rolService:RolService
    ){}
    @Get()
    async getAllRol() {
        const data = await this.rolService.getAll();
        return {
            message: 'Todos los Roles',
            data
        }
    }
    @Get(':id')
    async getOneRol(@Param('id') idRol:string) {
        const data = await this.rolService.getOne(idRol);
        return {
            message: `Un Rol con el id ${data.id}`,
            data
        }
    }
    @Post()
    async createOneRol(@Body() rolDto:CreateRolDto){
        const data = await this.rolService.createOne(rolDto);
        return {
            message: 'Crea un rol',
            data
        }
    }
    @Put(':id')
    async editOneURol(@Param('id') idRol:string,@Body() rolDto:UpdateRolDto){
        const data = await this.rolService.editOne(idRol,rolDto);
        return {
            message: `Edita un solo rol con el id ${data.id}`,
            data
        };
    }
    @Delete(':id')
    async markDeleteOneUser(@Param('id') idRol:string){
        const data = await this.rolService.markDeleteOne(idRol);
        return {
            message: `Elimina un solo rol con el id ${idRol}`,
            data
        };
    }
    @Delete('_delete/:id')
    async deleteOneUser(@Param('id') idRol:string){
        const data = await this.rolService.deleteOne(idRol);
        return {
            message: `Elimina un solo rol con el id ${idRol}`,
            data
        };
    }
}
