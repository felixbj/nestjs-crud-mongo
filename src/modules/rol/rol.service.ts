import { BadRequestException, ConflictException, Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { plainToClass } from 'class-transformer';
import { exception } from 'console';
import { Model } from 'mongoose';
import { CreateRolDto, ReadRolDto, UpdateRolDto } from './dto';
import { Rol,RolDocument } from './schema';

@Injectable()
export class RolService {
    constructor(
        @InjectModel(Rol.name)
        private readonly rolModel: Model<RolDocument>,
    ) {}
    async getAll():Promise<ReadRolDto[]>{
        return (await this.rolModel.find()).map(
            (rol:Rol) => plainToClass(ReadRolDto,rol)// class-transformer pasar la clase user a un dto
        );
    }
    async getOne(id:string,):Promise<ReadRolDto>{
        if (!id) throw new BadRequestException();
        const rol = await this.rolModel.findOne({'_id':id});
        if (!rol) throw new NotFoundException()
        return plainToClass(ReadRolDto,rol);
    }
    async createOne(rol:CreateRolDto):Promise<ReadRolDto>{
        if(!rol) throw new BadRequestException()
        const f =  await this.rolModel.findOne({ name:rol.name });
        if(f) throw new ConflictException()
        const newRol = new this.rolModel(rol);
        return plainToClass(ReadRolDto,await newRol.save());
    }
    async editOne(id:string,rol:UpdateRolDto):Promise<ReadRolDto>{
        if(!rol) throw new BadRequestException()
        const data = await this.rolModel.findOne({'_id':id});
        if (!data) throw new NotFoundException()
        const editedRol = Object.assign(data,rol);
        return plainToClass(ReadRolDto, await editedRol.save());
    }
    async markDeleteOne(id:string):Promise<ReadRolDto>{
        if (!id) throw new BadRequestException();
        const rol = await this.rolModel.findOne({'_id':id});
        if (!rol) throw new NotFoundException()
        const editedRol = Object.assign(rol,{ isActive: false });//marca de elimnado
        return plainToClass(ReadRolDto, await editedRol.save());
    }
    async deleteOne(id:string):Promise<Boolean>{
        if (!id) throw new BadRequestException();
        const data = await this.rolModel.findOne({ '_id':id,isActive:false });
        if (!data) throw new NotFoundException()
        const deleted = await this.rolModel.findByIdAndRemove(id)
            .then(deleted => { 
                return (deleted)?true:false;
            })    
            .catch(error => { return error; });
        if(deleted.error) throw new exception(deleted.error)
        return deleted;
    }
}
