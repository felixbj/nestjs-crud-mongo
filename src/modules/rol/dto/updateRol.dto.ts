import { IsNotEmpty, IsOptional } from "class-validator";
import { RolAccess,RolStatus } from "../enum";

export class UpdateRolDto {
    @IsNotEmpty()
    @IsOptional()
    readonly name: string;
    @IsNotEmpty()
    @IsOptional()
    readonly description: string;
    @IsNotEmpty()
    @IsOptional()
    readonly access: RolAccess;
    @IsNotEmpty()
    @IsOptional()
    readonly status: RolStatus;
    @IsNotEmpty()
    @IsOptional()
    readonly isActive: boolean;
}