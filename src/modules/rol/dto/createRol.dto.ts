import { IsEnum,IsNotEmpty } from "class-validator";
import { RolAccess,RolStatus } from "../enum";

export class CreateRolDto {
    @IsNotEmpty()
    readonly name: string;
    @IsNotEmpty()
    readonly description: string;
    @IsNotEmpty()
    @IsEnum(RolAccess)
    readonly access: RolAccess;
    @IsNotEmpty()
    @IsEnum(RolStatus)
    readonly status: RolStatus;
    @IsNotEmpty()
    readonly isActive: boolean;
}