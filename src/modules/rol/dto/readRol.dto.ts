import { IsNumber, IsString } from "class-validator";
import { Exclude,Expose } from "class-transformer"
import { RolStatus,RolAccess } from "../enum";

@Exclude()
export class ReadRolDto {
    @Expose()
    @IsNumber()
    readonly id: number;
    @Expose()
    @IsString()
    readonly name: string;
    @Expose()
    @IsString()
    readonly description: string;
    @Expose()
    @IsString()
    readonly access: RolAccess;
    @Expose()
    @IsString()
    readonly status: RolStatus;
}