import { registerAs }from '@nestjs/config';
import { MongooseModuleOptions } from '@nestjs/mongoose';

function createMongooseOptions(): MongooseModuleOptions {
    return {
        uri: process.env.MONGODB_URI,
        connectionName: process.env.MONGODB_CONN_NAME,
        useUnifiedTopology : true,
        useNewUrlParser: true,
        useCreateIndex: true,
    };
}

export default registerAs('database',() =>({
    config: createMongooseOptions()
}));// funcion para regstrar un namespace, congurar una varble de entorno llamada database
/**
 * uri?: string;
 * retryAttempts?: number;
 * retryDelay?: number;
 * connectionName?: string;
 * connectionFactory?: (connection: any, name: string) => any;
 */