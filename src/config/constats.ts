export const MONGOOSE_CONFIG = 'database.config';

export const USER_ADMINISTER = 'ADMINISTER'
export const USER_PUBLISH = 'PUBLISH'
export const USER_VIEWER = 'VIEWER'
export const USER_ACTIVE = 'ACTIVE'
export const USER_INACTIVE = 'INACTIVE'

export const ROL_ACTIVE = 'ACTIVE'
export const ROL_INACTIVE = 'INACTIVE'
export const DEFAULT_ROL = 'VIEWER'

export const BCRYPT_SALT = 11