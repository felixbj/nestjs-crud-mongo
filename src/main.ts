import { Logger,ValidationPipe } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const logger = new Logger('NestApplicationDetails',true);
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);// para usar variables de entorno
  const port = configService.get('APP_PORT');

  app.setGlobalPrefix('api/v0.3');// para agregar un prefijo global a la app
  app.useGlobalPipes(new ValidationPipe({
    whitelist:true
  }));// validar los dto encada end-poin
  
  await app.listen(port);
  logger.log(`The server is on ${ await app.getUrl() }`);
}
bootstrap();
