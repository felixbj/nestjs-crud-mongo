import { Injectable,Logger } from "@nestjs/common";
import { Seeder } from "nestjs-seeder";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Rol,RolDocument } from "src/modules/rol/schema";
import * as rolAccess from "src/modules/rol/enum/rolAccess.enum";
import { ROL_ACTIVE } from "src/config/constats";
 
@Injectable()
export class RolsSeeder implements Seeder {
    constructor(
        @InjectModel(Rol.name)
        private readonly rolModel:Model<RolDocument>,
    ) {}

    async seed(): Promise<any> {
        const logger = new Logger('Seeder',true);
        for(let rolAccessValue in rolAccess.RolAccess){
            if (isNaN(Number(rolAccessValue))) {
                const rol = {
                    name: rolAccessValue,
                    description: `${ rolAccessValue } DESCRIPTION`,
                    access:  rolAccessValue,
                    status: ROL_ACTIVE,
                    isActive: true,
                };
                const newRol = new this.rolModel(rol);
                await newRol.save();
            }
        }
        logger.log('RolSeeder completed');
        return true;
    }

    async drop(): Promise<any> {
        return false;
    }
}