import { Injectable,Logger } from "@nestjs/common";
import { Seeder } from "nestjs-seeder";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from "mongoose";
import { Profile,ProfileDocument,User,UserDocument } from "src/modules/user/schema";
import { Rol,RolDocument } from "src/modules/rol/schema";
import {genSalt, hash} from 'bcryptjs';
import { BCRYPT_SALT,USER_ACTIVE,USER_ADMINISTER } from 'src/config/constats';

@Injectable()
export class AdminSeeder implements Seeder {
    constructor(
        @InjectModel(User.name)
        private readonly userModel:Model<UserDocument>,
        @InjectModel(Profile.name)
        private readonly profileModel: Model<ProfileDocument>,
        @InjectModel(Rol.name)
        private readonly rolModel:Model<RolDocument>,
    ) {}

    async seed(): Promise<any> {
        const logger = new Logger('Seeder',true);
        const user = {
            username:"admin",
            email:"administrador@mail.com",
            password:"password",
            accesss:USER_ADMINISTER,
            status:USER_ACTIVE
        };
        const rol = await this.rolModel.findOne({'access':USER_ADMINISTER,'isActive':true});
        const newUser = new this.userModel(Object.assign({},user,{ role:[rol] },{ password:await hash (user.password, await genSalt (BCRYPT_SALT)) }));
        const profile = new this.profileModel({ idUser:newUser });
        await newUser.save();
        await profile.save();
        return true;
    }

    async drop(): Promise<any> {
        return false;
    }
}