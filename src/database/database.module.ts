import { Module } from '@nestjs/common';
import { MongooseModule, MongooseModuleOptions } from '@nestjs/mongoose';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MONGOOSE_CONFIG } from 'src/config/constats';

@Module({
    imports: [
        MongooseModule.forRootAsync({
            imports: [ConfigModule],
            inject: [ConfigService],
            useFactory:async (configService:ConfigService) => configService.get(MONGOOSE_CONFIG)
        })
    ]
})
export class DatabaseModule { }
// Modulo para configuracion de la base de datos