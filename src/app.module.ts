import { RolModule } from './modules/rol/rol.module';
import { UserModule } from './modules/user/user.module';
import { DatabaseModule } from './database/database.module';
import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import databaseConfig from './config/database.config';

@Module({
  imports: [
    RolModule,
    UserModule,
    DatabaseModule,
    ConfigModule.forRoot({
      load: [databaseConfig],// cargar la confguracon de la base de datos --- varble de entorno llamada database
      isGlobal: true,
      envFilePath: '.env'
    })
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
// crear el directorio config y un archivo constants.ts
//  Para almacenar las variables de configuración en el entorno npm i --save @nestjs/config e importamos ConfigModule
//  Configuramos ConfigModule
// crear directorio database ademas de modole
// Para la base de datos imstalar npm install --save @nestjs/mongoose mongoose npm install --save-dev @types/mongoose e importamos a databasemodule
// crear en el directorio config un archivo para la configuracion de la base de datos database.config
// Para magejar los dto intalar npm i --save class-validator class-transformer
//  usando Auto-validation creamos un ValidationPipe global para la aplicacion en main
// crear directorio modules/user, modules/rol
// Para crear seeders, instalar npm install nestjs-seeder --save-dev
//  Creamos en database el directorio seeders
//  Para reguistrar crear el archivo seeder.ts en la raiz del proyeto
//  incluir en package los scripts
//     "seed": "node dist/seeder",
//     "seed:refresh": "node dist/seeder --refresh"
